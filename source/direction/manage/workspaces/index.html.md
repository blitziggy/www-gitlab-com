---
layout: markdown_page
title: "Category Direction - Workspaces"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [Workspaces](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWorkspaces) |

## Introduction and how you can help
Thanks for visiting this category page on Workspaces within GitLab. This page belongs to the Spaces group of the Manage stage, and is maintained by [Luca Williams](https://gitlab.com/tipyn) who can be contacted directly via [email](mailto:luca@gitlab.com). You can also [open an issue](https://gitlab.com/gitlab-org/gitlab/issues) and @mention them there. Please add the `group::spaces` label for wider visibility.

This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute to this vision. If you’re a GitLab user and would like to talk to us about building a more autonomous and holistic experience for larger Enterprises on GitLab.com, we’d especially love to hear from you. 

With the fast-paced proliferation of Cloud services and more and more companies taking advantage of no longer needing to manage their own infrastructure, it's important to acknowledge and highlight the demand for scalable SaaS products that shape and define the way Enterprises work every day. Historically, GitLab has focused heavily on our Self-Managed offering as the best solution for Enterprise customers. Now that we've grown with our customer base, we need to focus on the importance of providing a SaaS platform which can not only safely and securely house our customers and users, but also deliver a rich and fulfilling Enterprise experience that looks and feels exactly like their own private instance of GitLab. 

Below is a recording on our GitLab Unfiltered channel of a discussion between some of the Manage Stage GitLabbers that highlights some of the pain points of SaaS customers not having this today. 

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/jsnx4Y2mUS0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## So, where do we start?

The first baby steps towards our larger vision is to improve Enterprise isolation, security and privacy. In order to better accommodate Enterprise customers, we need to build an additional, secure layer around Groups, Projects and the teams of people working in them in order to give them the space they need to spread out, work, create and communicate with each other. We will investigate how we can further extend Group Managed Accounts and iterate from there. Some of these steps include, but are not necessarily limited to: turning off any configuration that allows SaaS group owners to expose their group to the public, and enabling customers to enforce domain-specific logins to their groups via their own dedicated/branded login page (possibly with a custom URL) without requiring SSO to be configured. 

<!-- To add later - [sketch of personal .com login page with custom URL] -->

From there, we want to close the gap between System/Instance Admins and Group Owners by introducing a _new_ role that drops administrative functionality down a step, but not so far down that it compromises security. It's important that "Space" Administrators have the autonomy to enforce rules and set their own policies, protect their data and autonomously manage the people and activity happening within their Space. 

## What's the long-term vision?

Looking through the telescope a few years away, what we hope to achieve is a feature-rich integrated platform, or "Space", within GitLab that allows teams (and possibly also entire organisations) to communicate and collaborate effectively above and across many groups and projects. 

<!-- To add later - [sketches of "what this could be" vision] -->

Some ideas of what this could look like includes features such as Team activity feeds; Full and autonomous user management and unified access control; Strong privacy features and autonomous data protection; Productivity highlights and hotspots in your organisation; Configurable and informative dashboards; A blog platform; Scheduling; Integrated chat functionality and more. The goal is to move even further towards a single application by enriching the user experience in such a way that you never need to leave GitLab to get the job done.

## But... what about Self-Managed? 

Our vision for Spaces, despite _prioritising_ our SaaS customers, does not neglect our Self-Managed customers. We hope that this will also greatly benefit those folks who seek a barrier of separation between departments that are hosted on the same instance. We have customers today who are expressing this need. Our customers want a tighter, more granular way to manage their departments or child companies without having the additional overhead of hosting multiple instances. Hopefully, Spaces will help our Self-Managed customers to mitigate overlap and privacy compromises and more accurately budget and plan across their whole organisation. 

## Target audience and experience

Whilst we can assume that all users would ultimately benefit from this unifying, "Space" experience, the target personas we are focusing on are Team Leaders, [Group Owners](https://docs.gitlab.com/ee/user/permissions.html#permissions) & System Administrators.

### Current focus

We are about to wrap up the problem validation and customer interviewing which has allowed us to better understand the problems we need to initially solve around security and scalability. You can see the progress of this work [here](https://gitlab.com/groups/gitlab-org/-/epics/2447).

Next, we will be working on some designs of what our end goal as described above might look like to better communicate our vision as well as any potential designs that may be required for the first iteration. You can take a peek at the [parent epic](https://gitlab.com/groups/gitlab-org/-/epics/2446) for our research and design where we will also be handling solution validation for our first iteration before we confidently hand over to our engineering team. 

You can read more about our product development flow [here](https://about.gitlab.com/handbook/product-development-flow/).

<!-- ## What's next & why -->

## Maturity

For the moment, Workspaces are considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions. 

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/603).
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=user%profile&label_name[]=Accepting%20merge%20requests)!

<!-- ## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD

-->
