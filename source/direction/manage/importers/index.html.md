---
layout: markdown_page
title: "Category Direction - Importers"
---

- TOC
{:toc}

Last Reviewed: 2020-02-11

## Importers

Thanks for visiting the direction page for Importers in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2248) for this category.

## Problems to solve

A typical organization looking to adopt GitLab might already have work in progress. Artifacts such as code, build pipelines, issues and roadmaps may already exist and are being used daily. Seamless transition of work in progress is critically important and a great experience during this migration creates a positive first impression of GitLab. 

Currently, GitLab importers have a lot of room for improvement in user experience. Additionally, there are still many tools for which GitLab doesn't offer importers. Our importers are also limited to importing code and issues. However, there is a strong demand for the ability to migrate pipeline definitions into GitLab CI.

The mission of GitLab importers is solving these and other similar challenges.

## What's next & why

At the moment, the Import group is focused on enabling GitLab.com adoption through the introduction of [group import/export](https://gitlab.com/groups/gitlab-org/-/epics/1952). Once group import/export is completed, we will be exploring possible solutions for importing Jenkins configurations into GitLab CI in order to support a more automated migration of customers from Jenkins to GitLab. Additionally, our UX group is looking into the overall import user experience in order to create a more positive first impression when migrating to GitLab.

While this group focuses on building importers, it can often happen that importers are a higher priority for another group in gaining adoption of their features. When this happens other teams should not wait for their importer to become a priority of the Import group, but should just prioritize the work themselves since [everyone can contribute](https://about.gitlab.com/handbook/values/#mission).

## Maturity plan

Our maturity plan is currently under construction. If you'd like to contribute feedback on areas you'd like to see prioritized, please add them as comments in the [epic](https://gitlab.com/groups/gitlab-org/-/epics/2248).
