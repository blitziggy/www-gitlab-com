---
layout: handbook-page-toc
title: "Engineering Management"
---

## How Engineering Management Works at GitLab

At GitLab, we promote two paths for leadership in Engineering. While there is a
healthy degree of overlap between these two ideas, it is helpful and efficient
for us to specialize training and responsibility for each of:

- **Technical leadership**, as represented by [Staff and higher-level engineers](/handbook/engineering/career-development#roles).
- **Professional leadership**, as represented by [Engineering management](/handbook/engineering/career-development#roles).

While technical leadership tends to come naturally to software engineers,
professional leadership can be more difficult to master. 

This page will serve as a training resource and operational guide for current and future managers.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## General leadership principles

All Engineering Managers should follow the [general leadership
principles](/handbook/leadership/) set out in the handbook. In particular, it is
not uncommon for Engineering Managers to struggle with one or more of the
following areas, so we recommend you review them carefully and discuss your
confidence with your manager:

- [1-1s](/handbook/leadership/1-1/)
- [Providing regular feedback](/handbook/leadership/#giving-performance-feedback)
- [Dealing with underperformance](/handbook/underperformance/)

## Engineering Manager Onboarding

Onboarding is essential for all Engineering Managers at GitLab. As part of onboarding, a [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) issue will be created for each manager when they join, or are promoted. This issue is intended to connect new managers with the crucial information they need, and ensure they have access to all the resources and training available.

## Technical Credibility

We expect all managers at GitLab to be technically credible with their teams.
Fluency in our core technologies and architectures is essential because it
enables managers to participate effectively on technical conversations. In order
to maintain this fluency, we encourage managers to participate in coding-related
work to an extent. However, please keep the following advice in mind:

- Avoid critical path work. If work has been scheduled for a release or is
  otherwise blocking other members of the team, it's best left to a developer
  who can focus on it more holistically. As a manager you should expect regular
  interruptions to your day that will make you less effective on this kind of
  task.
- Focus on where you can add the most value. As mentioned above, you won't add
  value on critical path work, but that doesn't mean you can't add value in
  other ways. For a great discussion on this, see this article on [How (and why)
  Should Managers Code](https://medium.com/@johnbarton/how-and-why-should-managers-code-323751799664).
- Plan to review more than you write. Following from all of this, most of your
  "coding-related work" should be in code review - you add more value reviewing
  code to stay up to date with your team and provide them feedback and guidance.
  If you find yourself spending more time writing code than you do reviewing it,
  this may be an indicator that you need to revisit your priorities as a leader.

## Management Responsibilities

The sections below aim to inform you of the responsibilities that an 
engineering manager has at GitLab. It will provide you with the necessary context,
information, and process to follow.

- [Hiring](/handbook/engineering/management/hiring/)
- [Career Development](/handbook/engineering/management/career-development/)
- [Project Management](/handbook/engineering/management/project-management/)
- [Team Retrospectives](/handbook/engineering/management/team-retrospectives/)
- [Engineering Metrics](/handbook/engineering/management/engineering-metrics/)
