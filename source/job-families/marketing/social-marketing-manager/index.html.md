---
layout: job_family_page
title: "Social Marketing Manager"
---

The world’s best brands feel more like a friend to their audience than a company.
This is because the world’s best brands understand people, and weave that understanding
into everything they do. The Social Marketing Manager is a key part of building GitLab’s
brand into one that is loved by its audience of developers, IT ops practitioners, and IT leaders.

This role will focus on understanding people who make up the GitLab community,
including those who use GitLab (or are potential users), sharing content over social
channels that they’ll find useful and valuable, and helping the rest of the marketing
department understand the people that make up the markets that GitLab is serving.

## Responsibilities

- Day-to-day publishing and reporting
- Grow social media following and engagement
- Generate and edit social content
- Design and execute social media strategy and campaigns across channels
- Support integrated marketing campaigns, events, initiatives
- Perform research on current benchmark trends and audience preferences

## Requirements

- In-depth knowledge and understanding of social media platforms and their respective uses including Twitter, Facebook, LinkedIn, and Instagram
- Excellent writing and communication skills
- Highly collaborative. This role will require effective collaboration across multiple teams, organizations, and individuals.
- Extremely detail-oriented and organized
- Ability to confidently write and engage in the GitLab brand voice and personality in real time
- Ability to use GitLab
- You share our [values](/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

## Levels

### Social Marketing Associate

- Day-to-day publishing and reporting
- Social listening: Monitor conversations about GitLab online to flag positive brand opportunities that can be converted into meaningful marketing materials via offline activities and user-generated content (blog posts, speaking opps, interviews, etc.)
- Assist Community Advocates in flagging negative conversations and brand associations
- Work with the team to ensure social amplification and engagement is incorporated as part of integrated campaign strategy and execution
- Handle reporting for social media initiatives, including channel health, campaign, and launch reports

#### Requirements

- 1-3 years of experience in a social media related role
- Proven ability to write effective short form content
- Able to coordinate with multiple stakeholders and perform in a fast-moving start-up environment
- Proven ability to work on multiple projects at a time

### Social Marketing Manager

All of the responsibilities of an associate, plus:

- Own strategic social media planning in support of product launches, company news, crisis communications, and brand campaigns by determining what to publish, when, and where.
- Manage the social calendar and craft social campaigns that will help engage, grow and educate our followers
- Consult with interested team members on their own social strategy, and how they can help position themselves within the GitLab community
- Be the voice of our consumers to our internal teams. Advocate for their needs, represent their feedback, and flag opportunities to do more, and identify potential pitfalls
- Grow GitLab’s social fan base while ensuring the quality of our followers & engagements
- Contribute to quarterly OKRs and initiatives

#### Requirements

- 3-5 years experience in a social marketing-related role
- Enterprise software marketing experience
- Proven ability to manage social media channels
- Proven ability to report on channel performance

### Senior Social Marketing Manager

All of the responsibilities of a manager, plus:

- Independently develop and execute social marketing growth campaigns
- Articulate & document social marketing strategy and process in the GitLab handbook
- Entrust work to other members of the social marketing team as appropriate
- Responsible for ideation of social marketing initiatives, OKRs, and reporting on results
- Identify key social influencers and work to develop and cultivate relationship and engagement
- Identify new channels and audiences to expand to
- Develop and manage social enablement program

#### Requirements

- 5+ years in a social media-related role
- 3+ years of enterprise software marketing experience
- Proven experience growning B2B social media networks
- In-depth industry and technical community knowledge
- Proven ability to identify new audiences and channels, create a strategy, and execute the strategy

### Manager, Social Marketing

Managers in the Social Marketing team are prolific and creative social media strategists. They define GitLab’s social media marketing strategy, and manage a world-class team to expand GitLab's influence, awareness, subscribers, and leads. They own the delivery and results of Social Marketing deliverables and OKRs. They are focused on improving results, productivity, and operational efficiency. They must also collaborate across departments to accomplish collaborative goals.

#### Responsibilites

- Define, implement, and regularly iterate on GitLab's social media marketing strategy
- Develop and implement a content framework for managing social content development, publishing, amplification, and measurement
- Oversee GitLab's social marketing initiatives
- Build and expand audience reach and engagement via a strategic publishing plan
- Collaborate with corporate communications on positioning and messaging
- Collaborate with Digital Programs on campaign strategy
- Perform regular gap analysis to identify areas of success, improvement, and opportunity
- Hire and manage a world class team of social marketers
- Hold regular 1:1s with all members of the team

#### Requirements

- Degree in marketing, journalism, communications or a related field
- 8+ years in a social media-related role
- 5+ years of enterprise software marketing experience
- Proven experience growing B2B social media networks
- Experience defining the high-level strategy and creating social plans based on research and data
- In-depth industry and technical community knowledge
- Strong communication skills without fear of over communication. This role will require effective collaboration and coordination across internal and external contributors
- Regular reporting on channel performance
- BONUS: A passion and strong understanding of the industry and our mission.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Candidats will submit links to social media profiles they have worked on and 2-3 short form writing samples.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Manager, Content Marketing, Social Marketing Manager, and Community Advocate
- Candidates will then be invited to schedule 45-minute interviews with our Senior Director of Corporate Marketing and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
