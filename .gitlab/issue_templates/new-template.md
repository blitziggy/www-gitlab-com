
## This template is for requesting new templates or generators.

This repository primarily relates to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/). This is not the right repository for requests related to docs.gitlab.com, product, or other parts of the site.

# Issues should identify [The Five W's](https://en.wikipedia.org/wiki/Five_Ws) : who, what, when, where, and why.

It is also recommended to apply [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) sorting to your requests (must, should, could, won't).

#### Please apply appropriate labels or your issue may not be sorted on to appropriate boards.

Please refer to the [website label documentation](/handbook/marketing/brand-and-digital-design/#issue-labels)

## Please delete the lines above and any irrelevant sections below.

Below are suggestions. Any extra information you provide is beneficial.

#### Briefly describe the desired template.

(Example: Take this preexisting landing page and turn it into a template for similar pages like X and Y.)

#### What pages does this template create?

Please provide specific URLs and/or filenames.

#### What data does this template need?

(Example: For each templated page, display date, title, location, venue, schedule, speakers...)

#### Where is that data sourced from?

(Example: events.yml)

#### What is the primary purpose of this template?

(Example: Sign up for events.)

#### What are the KPI (Key Performance Indicators)

(Example: Number of signups.)

#### What other purposes does this template serve, if any?

(Example: Showcase speakers and sponsors.)

### Who is the primary audience? Be specific.

(Example: Potential customers vs existing vs developers vs speakers vs CTO )

#### Any other audiences?

(Example: Media outlets, investors, etc)

#### Where do you expect traffic to come from?

(Example: Homepage direct link, on-site blog post, social media campaign, targeted ads)

#### If applicable, are there any items which can be hidden at first glance but still need to be present?

(Example: Tooltips, tabbed sections, collapsible sections, filterable items, different view-type toggles, view-more buttons, extra data)

#### Please provide some example websites for design guidance.

(Example: "Pinterest implements recommendations well", "Stripe has good payment UX", "I like the aesthetics of nike.com", animation examples, microinteractions, etc)

#### Does this page/flow require any events, parameters, or tracking?

(Example: Clicking button X should record a purchase into analytics)

#### Does this page/flow impact or need any reports?

(Example: This impacts the Retention dashboard in Periscope)

#### Does this page/flow require any automation?

(Example: Create a test ensuring a job listing is never empty)

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"mktg-website" ~"template::web-template" ~"mktg-status::triage"

<!-- If you do not want to ping the website team, please remove the following section -->
/cc @gl-website
